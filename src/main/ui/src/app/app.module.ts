import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LayoutComponent} from './view/layout/layout.component';
import {HeaderComponent} from './view/layout/header/header.component';
import {FooterComponent} from './view/layout/footer/footer.component';
import {TestComponent} from './view/test/test.component';
import {AppRoutingModule} from './app-routing.module';
import {CategoryListComponent} from './view/category/category-list/category-list.component';
import {HttpClientModule} from '@angular/common/http';
import {CategoryComponent} from './view/category/category.component';
import {CategoryFormComponent} from './view/category/category-form/category-form.component'
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProductComponent} from "./view/product/product.component";
import {ProductListComponent} from "./view/product/product-list/product-list.component";
import {ProductFormComponent} from "./view/product/product-form/product-form.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import {CartComponent} from "./view/cart/cart.component";
import {LoginFormComponent} from "./view/user/login-form/login-form.component";
import { ProfileComponent } from './view/user/profile/profile.component';
import { LoginHistoryComponent } from './view/user/login-history/login-history.component';
import {AngularDateTimePickerModule} from "angular2-datetimepicker";
import { AdminComponent } from './view/admin/admin.component';
import {PurchaseListComponent} from "./view/user/history/purchase-list/purchase-list.component";
import { PurchaseDetailsComponent } from './view/user/history/purchase-details/purchase-details.component';
import { ShopComponent } from './view/product/shop/shop.component';
import { ProductListTableComponent } from './view/product/product-list-table/product-list-table.component';
import { ProductDetailsComponent } from './view/product/product-details/product-details.component';
import { UserComponent } from './view/user/user.component';
import { UserListTableComponent } from './view/user/user-list-table/user-list-table.component';
import { UserFormComponent } from './view/user/user-form/user-form.component';
import { UserRegisterComponent } from './view/user/user-register/user-register.component';

@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
        HeaderComponent,
        FooterComponent,
        TestComponent,
        CategoryListComponent,
        CategoryComponent,
        CategoryFormComponent,
        ProductComponent,
        ProductListComponent,
        ProductFormComponent,
        CartComponent,
        LoginFormComponent,
        ProfileComponent,
        LoginHistoryComponent,
        AdminComponent,
        PurchaseListComponent,
        PurchaseDetailsComponent,
        ShopComponent,
        ProductListTableComponent,
        ProductDetailsComponent,
        UserComponent,
        UserListTableComponent,
        UserFormComponent,
        UserRegisterComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        FontAwesomeModule,
        AngularDateTimePickerModule,
        NgbModule.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
