import {Component, OnInit} from '@angular/core';
import {Category} from '../../models/category';
import {CategoryService} from "../../services/category.service";

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

    cat: Category;
    visibleForm: boolean = false;
    message: string;
    categories: Category[];

    constructor(private categoryService: CategoryService) {
    }

    ngOnInit() {
        this.loadCategories();
    }

    addCategory() {
        this.visibleForm = true;
    }

    categoryWasSelected(cat: Category) {
        this.cat = cat;
        this.visibleForm = true;
    }

    actionCompleted(bool: boolean) {
        if (bool) {
            this.visibleForm = false;
            this.loadCategories();
            this.cat = undefined;
        }
        else {
            this.visibleForm = true;
            this.message = 'Error! Category not saved!';
            setTimeout(function () {
                this.message = '';
            }.bind(this), 5000);
        }
    }

    private loadCategories() {
        this.categoryService.getCategories()
            .subscribe(data => {
                this.categories = data;
            });
    }
}
