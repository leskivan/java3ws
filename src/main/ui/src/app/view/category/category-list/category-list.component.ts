import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Category } from '../../../models/category';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  @Output() selectCategory: EventEmitter<Category>;
  @Input() categories: Category[];

  constructor() {
    this.selectCategory = new EventEmitter();
  }

  selectItem(cat: Category) {
    this.selectCategory.emit(cat);
  }

  ngOnInit() {
  }
}
