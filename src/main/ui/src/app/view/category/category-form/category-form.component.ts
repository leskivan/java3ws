import {Component, OnInit, Input, SimpleChanges, SimpleChange, Output, EventEmitter} from '@angular/core';
import {Category} from '../../../models/category';
import {CategoryService} from "../../../services/category.service";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-category-form',
    templateUrl: './category-form.component.html',
    styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit {

    @Input() category: Category;
    @Output() completed = new EventEmitter();

    categoryForm: FormGroup;

    constructor(private categoryService: CategoryService, private fb: FormBuilder) {
        this.categoryForm = fb.group({
            'name': []
        });
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (this.category !== undefined) {
            this.categoryForm.setValue({
                'name': this.category.name
            });
        }
    }

    saveCategory() {

        if (this.category === undefined || this.category === null) {
            this.category = new Category(null, "");
        }
        this.category.name = this.categoryForm.value.name;
        this.categoryService.saveCategory(this.category)
            .subscribe(
                (data) => {
                    this.category = new Category(null, "");
                    this.categoryForm.reset();
                    this.completed.emit(true);
                },
                (error) => {
                    console.log(error);
                });
    }

    deleteCategory() {
        if (this.category.id != 0) {
            this.categoryService.deleteCategory(this.category.id)
                .subscribe(
                    (data) => {
                        this.categoryForm.reset();
                        this.completed.emit(true);

                    },
                    (error) => {
                        console.log(error);
                    }
                );
        }
    }
}
