import {Component, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {ProductService} from "../../services/product.service";

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

    products: Product[];
    selectedProduct: Product;
    visibleForm: boolean = false;
    message: string;

    constructor(private productService: ProductService) {
    }

    ngOnInit() {
        this.loadProducts();
    }

    selectProduct(product: Product) {
        this.selectedProduct = product;
        this.visibleForm = true;
    }

    deleteProduct(product: Product) {
        this.productService.deleteProduct(product.id).subscribe(
            (data) => {
                this.loadProducts();
            },
            (error) => {
                console.log(error);
                this.message = 'Error deleting product!';
                setTimeout(function () {
                    this.message = '';
                }.bind(this), 5000);
            }
        );
    }

    actionCompleted(bool: boolean) {
        if (bool) {
            this.loadProducts();
            this.visibleForm = false;
            this.selectedProduct = undefined;
        }
        else {
            this.visibleForm = true;
            this.message = 'Error! Product not saved!';
            setTimeout(function () {
                this.message = '';
            }.bind(this), 5000);
        }
    }

    addProduct() {
        this.visibleForm = true;
    }


    loadProducts() {
        this.productService.getAllProducts().subscribe(
            (data) => {
                this.products = data;
            },
            (error) => {
                console.log(error)
            }
        );
    }

}
