import {Component, OnInit} from '@angular/core';
import {Category} from "../../../models/category";
import {CategoryService} from "../../../services/category.service";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {ProductService} from "../../../services/product.service";
import {Product} from "../../../models/product";
import {b} from "@angular/core/src/render3";

@Component({
    selector: 'app-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

    categories: Category[];
    selectedCategories: Category[];
    products: Product[];
    allProducts: Product[];
    icon = faTimes;

    constructor(
        private categoryService: CategoryService,
        private productService: ProductService
    ) {
        this.categories = [];
        this.selectedCategories = [];
    }

    ngOnInit() {
        this.loadCategories();
        this.loadProducts();
    }

    loadCategories() {
        this.categoryService.getCategories().subscribe(
            (data) => {
                this.categories = data;
            },
            (error) => {
                console.log(error)
            }
        );
    }

    loadProducts() {
        this.productService.getProducts()
            .subscribe(data => {
                this.products = data;
                this.allProducts = data;
            });
    }

    unselectCategory(cat) {
        this.selectedCategories = this.selectedCategories.filter(obj => obj !== cat);
        this.showProducts();
    }

    categoryWasSelected(cat: Category) {
        this.selectedCategories.push(cat);
        this.selectedCategories = Array.from(new Set(this.selectedCategories));
        this.showProducts();
    }

    showProducts() {
        this.products = this.allProducts.filter(product => this.containsOneOrMoreCats(product.categories, this.selectedCategories));
    }

    containsOneOrMoreCats(allCats: Category[], selCats: Category[]): boolean {
        for (let myCat of allCats) {
            for (let outCat of selCats) {
                if (myCat.id == outCat.id) {
                    return true;
                }
            }
        }
        return false;
    }

}
