import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../../../services/product.service";
import {Product} from "../../../models/product";
import {faCartPlus} from "@fortawesome/free-solid-svg-icons";
import {CartService} from "../../../services/cart.service";

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

    id: number;
    private sub: any;
    product: Product;

    icon = faCartPlus;

    constructor(
        private route: ActivatedRoute,
        private productService: ProductService,
        private cartService: CartService
    ) {
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];
            this.loadProduct(this.id);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    loadProduct(id: number) {
        this.productService.getProductById(id).subscribe(
            (data) => {
                this.product = data;
            },
            (error) => {
                console.log(error)
            }
        );
    }

    addToCart(product: Product) {
        this.cartService.addToCart(product);
    }
}
