import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../../models/product";
import {faTimes, faCheck} from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: 'app-product-list-table',
    templateUrl: './product-list-table.component.html',
    styleUrls: ['./product-list-table.component.css']
})
export class ProductListTableComponent implements OnInit {

    @Input() products: Product[];
    @Output() productEdit = new EventEmitter();
    @Output() productDelete = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    editProduct(product: Product): void {
        this.productEdit.emit(product);
    }

    deleteProduct(product) {
        this.productDelete.emit(product);
    }

    deleteButtonIcon(product: Product) {
        if (product.status == "Active") {
            return faTimes;
        } else {
            return faCheck;
        }
    }
}