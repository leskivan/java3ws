import {Component, Input, OnInit} from '@angular/core';
import {Category} from "../../../models/category";
import {ProductService} from "../../../services/product.service";
import {Product} from "../../../models/product";
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import {CartService} from "../../../services/cart.service";

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

    @Input() products: Product[];
    faCoffee = faCartPlus;

    constructor(private cartService: CartService) {
    }

    ngOnInit() {
    }

    addToCart(product: Product) {
        this.cartService.addToCart(product);
    }

}
