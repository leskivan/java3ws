import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Product} from "../../../models/product";
import {ProductService} from "../../../services/product.service";
import {CategoryService} from "../../../services/category.service";
import {Category} from "../../../models/category";

@Component({
    selector: 'app-product-form',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

    productForm: FormGroup;
    categories: Category[];
    image_link = ""
    showImage: boolean = false;
    showImgError: boolean = false;

    @Input() product: Product;
    @Output() completed = new EventEmitter();

    constructor(
        private fb: FormBuilder,
        private productService: ProductService,
        private categoryService: CategoryService
    ) {
        this.productForm = fb.group({
            'name': ['', Validators.required],
            'desc': ['', Validators.required],
            'img': ['', Validators.required],
            'price': ['', Validators.required],
            'onstock': ['', Validators.required],
            'categories': ['', Validators.required]
        });

        this.productForm.valueChanges.subscribe(
            (form: any) => {
                Object.keys(form).forEach(key => {
                    if (key === 'img') {
                        if (this.isValidURL(form[key])) {
                            if (form[key] == null) {
                                this.image_link = '';
                                this.showImage = false;
                            }
                            else {
                                this.image_link  = form[key];
                                this.showImage = true;
                            }
                        }
                    }
                })
            }
        )
    }

    ngOnInit() {
        this.categoryService.getCategories().subscribe(
            (data) => {
                this.categories = data;
            },
            (error) => {
                console.log(error);
            }
        );
    }

    ngOnChanges() {
        if (this.product !== undefined) {
            let catIdArr: number[] = [];
            for (let cat of this.product.categories) {
                catIdArr.push(cat.id);
            }
            this.productForm.setValue({
                'name': this.product.name,
                'desc': this.product.descripion,
                'img': this.product.imageUrl,
                'price': this.product.price,
                'onstock': this.product.onStock,
                'categories': catIdArr
            });
        }
    }

    invalidUrl(){
        this.showImage = false;
        this.showImgError = true;
    }

    onSubmit() {
        let valueObj = this.productForm.value;
        let id: number = null;
        let categories: Category[] = this.getCategories(valueObj.categories);

        if (this.product !== undefined) {
            id = this.product.id;
        }

        let newProduct = new Product(id, valueObj.name, valueObj.desc, valueObj.price, valueObj.onstock, valueObj.img, null, "Active", categories);

        this.productService.saveProduct(newProduct).subscribe(
            (data) => {
                this.completed.emit(true);
                this.productForm.reset();
            },
            (error) => {
                console.log(error);
                this.completed.emit(false);
            }
        )
    }

    getCategories(categoryIds: number[]): Category[] {
        let catArray: Category[] = [];
        for (let category of this.categories) {
            if (categoryIds.includes(category.id)) {
                catArray.push(category)
            }
        }
        return catArray;
    }

    isValidURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return pattern.test(str);
    }
}
