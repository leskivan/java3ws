import {Component, OnInit} from '@angular/core';
import {CartService} from "../../../services/cart.service";
import {Cartitem} from "../../../models/cartitem";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    isNavbarCollapsed = true;

    cart: Cartitem[] = [];
    icon = faShoppingCart;
    isAdmin = false;

    constructor(private cartService: CartService, private authService: AuthService, public router: Router) {
    }

    ngOnInit() {
        this.cart = this.cartService.getCart();
        this.isAdmin = this.authService.isAdmin();
    }

    ngDoCheck() {
        this.isAdmin = this.authService.isAdmin();
        this.cart = this.cartService.getCart();
    }

    me() {
        return this.authService.getUserDetails();
    }

    logout() {
        this.authService.logout();
        this.router.navigate(['']);
    }
}
