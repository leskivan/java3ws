import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../../models/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user.service";
import {Category} from "../../../models/category";
import {Product} from "../../../models/product";

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

    @Input() user: User;
    @Output() completed = new EventEmitter();
    userForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private userService: UserService
    ) {
        this.userForm = fb.group({
            'username': ['', Validators.required],
            'password': ['', Validators.required],
            'email': ['', Validators.required],
            'name': ['', Validators.required],
            'surname': ['', Validators.required],
            'address': ['', Validators.required],
            'city': ['', Validators.required],
            'zip': ['', Validators.required],
            'country': ['', Validators.required],

        });
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (this.user !== undefined) {
            this.userForm.setValue({
                'username': this.user.username,
                'password': "",
                'email': this.user.email,
                'name': this.user.name,
                'surname': this.user.surname,
                'address': this.user.address,
                'city': this.user.city,
                'zip': this.user.zip,
                'country': this.user.country
            });
            this.userForm.controls.password.disable();
            this.userForm.controls.username.disable();
        }
        else {
            this.userForm.controls.password.enable();
            this.userForm.controls.username.enable();
        }
    }

    onSubmit() {
        let valueObj = this.userForm.value;
        let id: number = null;
        let password: string = null;

        if (this.user !== undefined) {
            id = this.user.id;
            valueObj.password = null;
            valueObj.username = this.user.username;
        }

        let newUser = new User(
            id,
            valueObj.username,
            valueObj.password,
            valueObj.email,
            valueObj.name,
            valueObj.surname,
            valueObj.address,
            valueObj.city,
            valueObj.country,
            valueObj.zip,
            null);

        this.userService.saveUser(newUser).subscribe(
            (data) => {
                this.completed.emit(true);
                this.userForm.reset();
            },
            (error) => {
                console.log(error);
                this.completed.emit(false);
            }
        )
    }
}
