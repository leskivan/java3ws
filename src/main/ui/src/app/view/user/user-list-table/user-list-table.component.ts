import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../../models/user";

@Component({
    selector: 'app-user-list-table',
    templateUrl: './user-list-table.component.html',
    styleUrls: ['./user-list-table.component.css']
})
export class UserListTableComponent implements OnInit {

    @Input() users: User[];
    @Output() userEdit = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    editUser(user: User){
        this.userEdit.emit(user);
    }
}
