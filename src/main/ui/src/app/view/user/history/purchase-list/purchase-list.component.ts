import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Purchase} from "../../../../models/purchase";

@Component({
    selector: 'app-purchase-list',
    templateUrl: './purchase-list.component.html',
    styleUrls: ['./purchase-list.component.css']
})
export class PurchaseListComponent implements OnInit {

    @Input() purchases: Purchase[];
    @Input() isAdmin: boolean;
    @Output() purchaseSelected = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
        if (this.isAdmin === undefined) {
            this.isAdmin = false;
        }
    }

    calculateAmount(purchaseItems) {
        let total: number = 0;
        for (let item of purchaseItems) {
            total = total + (item.product.price * item.quantity);
        }
        return total;
    }

    purchaseSelect(purchase: Purchase) {
        this.purchaseSelected.emit(purchase);
    }
}
