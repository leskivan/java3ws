import {Component, Input, OnInit} from '@angular/core';
import {Purchase} from "../../../../models/purchase";

@Component({
    selector: 'app-purchase-details',
    templateUrl: './purchase-details.component.html',
    styleUrls: ['./purchase-details.component.css']
})
export class PurchaseDetailsComponent implements OnInit {

    @Input() purchase: Purchase;
    @Input() isAdmin: boolean;

    constructor() {
    }

    ngOnInit() {
        if (this.isAdmin === undefined) {
            this.isAdmin = false;
        }
    }

    calculateAmount(purchaseItems?) {
        if (!purchaseItems) return 0;
        let total: number = 0;
        for (let item of purchaseItems) {
            total = total + (item.product.price * item.quantity);
        }
        return total;
    }
}
