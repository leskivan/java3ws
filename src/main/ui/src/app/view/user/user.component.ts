import {Component, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {User} from "../../models/user";
import {UserService} from "../../services/user.service";

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    users: User[];
    message: string;
    visibleForm: boolean = false;
    user: User;

    constructor(
        private userService: UserService
    ) {
    }

    ngOnInit() {
        this.loadUsers();
    }

    editUser(user: User) {
        this.user = user;
        this.visibleForm = true;
    }

    addUser() {
        this.visibleForm = true;
    }

    loadUsers() {
        this.userService.getUsers().subscribe(
            (data) => {
                this.users = data;
            },
            (error) => {
                console.log(error)
            }
        );
    }

    actionCompleted(bool: boolean) {
        if (bool) {
            this.loadUsers();
            this.visibleForm = false;
            this.user = undefined;
        }
        else {
            this.visibleForm = true;
            this.message = 'Error! User not saved!';
            setTimeout(function () {
                this.message = '';
            }.bind(this), 5000);
        }
    }
}
