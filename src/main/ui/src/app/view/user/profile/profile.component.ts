import {Component, OnInit} from '@angular/core';
import {LoginHistoryItem} from "../../../models/loginhistoryitem";
import {AuditService} from "../../../services/audit.service";
import {PurchaseService} from "../../../services/purchase.service";
import {Purchase} from "../../../models/purchase";

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    loginHistory: LoginHistoryItem[];
    purchases: Purchase[];

    constructor(private auditService: AuditService, private purchaseService: PurchaseService) {
    }

    dateStart: Date = new Date();
    dateEnd: Date  = new Date();
    dateStartHistory: Date = new Date();
    dateEndHistory: Date  = new Date();

    showLoginHistory: boolean = false;
    selectedPurchase: Purchase;

    settings = {
        bigBanner: true,
        timePicker: true,
        format: 'dd.MM.yyyy HH:mm',
        defaultOpen: false
    }

    ngOnInit() {
        this.dateStart.setHours(0,0,0,0);
        this.dateStartHistory.setHours(0,0,0,0);
    }

    onDateSelect($event) {
        this.showLoginHistory = true;
        this.auditService.getMyAudit(new Date(this.dateStart), new Date(this.dateEnd)).subscribe(
            (data) => {
                this.loginHistory = data;
            },
            error => {
                console.log(error);
            }
        );

    }

    getPurchases() {
        this.purchaseService.getPurchases(new Date(this.dateStartHistory), new Date(this.dateEndHistory)).subscribe(
            (data) => {
                this.purchases = data;
            },
            (error) => {
                console.log(error);
            }
        );
    }

    showPurchaseDetails(purchase: Purchase) {
        this.selectedPurchase = purchase;
    }
}
