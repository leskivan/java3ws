import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AuthService} from "../../../services/auth.service";
import {UserCredentials} from "../../../models/userCredentials";
import {Location} from '@angular/common';

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

    loginForm: FormGroup;
    message: string;
    userCred: UserCredentials;

    constructor(private authService:AuthService, private fb: FormBuilder, private _location: Location) {
        this.loginForm = fb.group({
            'username': [""],
            'password': [""]
        });
    }

    ngOnInit() {
    }

    login(): void {
        this.userCred = new UserCredentials(this.loginForm.value.username, this.loginForm.value.password);
        this.authService.attemptAuth(this.userCred)
            .subscribe(
                token => {
                    this.authService.setToken(token['accessToken']);
                    this.authService.loadUserDetails();
                    this._location.back();
                },
                error => {
                    console.log(error)
                    this.message = 'Wrong login data!';
                    setTimeout(function () {
                        this.message = '';
                    }.bind(this), 2500);
                }
            )
    }


}
