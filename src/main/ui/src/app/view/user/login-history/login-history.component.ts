import {Component, Input, OnInit} from '@angular/core';
import {Category} from "../../../models/category";
import {LoginHistoryItem} from "../../../models/loginhistoryitem";
import {b} from "@angular/core/src/render3";

@Component({
    selector: 'app-login-history',
    templateUrl: './login-history.component.html',
    styleUrls: ['./login-history.component.css']
})
export class LoginHistoryComponent implements OnInit {

    @Input() loginHistory: LoginHistoryItem[];
    @Input() isAdmin: boolean;
    constructor() {
    }

    ngOnInit() {
        if (this.loginHistory == undefined) {
            this.loginHistory = [];
        }
        if (this.isAdmin === undefined) {
            this.isAdmin = false;
        }
    }

}
