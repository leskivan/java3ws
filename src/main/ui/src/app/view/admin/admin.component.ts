import {Component, OnInit} from '@angular/core';
import {LoginHistoryItem} from "../../models/loginhistoryitem";
import {AuditService} from "../../services/audit.service";
import {PurchaseService} from "../../services/purchase.service";
import {User} from "../../models/user";
import {UserService} from "../../services/user.service";
import {b} from "@angular/core/src/render3";
import {Purchase} from "../../models/purchase";

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

    loginHistory: LoginHistoryItem[];
    purchases: Purchase[];
    users: User[];
    selectedUsername: string;
    selectedUsernamePurchase: string;
    message: string;
    showLoginHistory: boolean = false;
    showPurchase: boolean = false;
    selectedPurchase: Purchase;

    constructor(
        private auditService: AuditService,
        private purchaseService: PurchaseService,
        private userService: UserService) {
    }

    dateStart: Date = new Date();
    dateEnd: Date = new Date();
    dateStartHistory: Date = new Date();
    dateEndHistory: Date = new Date();

    settings = {
        bigBanner: true,
        timePicker: true,
        format: 'dd.MM.yyyy HH:mm',
        defaultOpen: false
    }

    ngOnInit() {
        this.selectedUsername = "";
        this.selectedUsernamePurchase = "";
        this.dateStart.setHours(0, 0, 0, 0);
        this.dateStartHistory.setHours(0, 0, 0, 0);

        this.userService.getUsers().subscribe(
            (users) => {
                this.users = users;
            },
            (error) => {
                console.log(error);
            }
        );
    }

    onDateSelect($event) {
        if (this.selectedUsername != "") {
            this.showLoginHistory = true;
            this.auditService.getAuditByUsername(this.selectedUsername, new Date(this.dateStart), new Date(this.dateEnd)).subscribe(
                (data) => {
                    this.loginHistory = data;
                },
                error => {
                    console.log(error);
                }
            );
        } else {
            this.message = 'Please select username!';
            setTimeout(function () {
                this.message = '';
            }.bind(this), 5000);
        }
    }

    getPurchases() {
        this.showPurchase = true;
        if (this.selectedUsernamePurchase != "") {
            this.purchaseService.getPurchasesByUsername(this.selectedUsernamePurchase, new Date(this.dateStartHistory), new Date(this.dateEndHistory)).subscribe(
                (data) => {
                    this.purchases = data;
                },
                (error) => {
                    console.log(error);
                }
            );
        } else {
            this.message = 'Please select username!';
            setTimeout(function () {
                this.message = '';
            }.bind(this), 5000);
        }
    }

    usernameChange($event) {
        this.selectedUsername = $event.target.value;
    }

    usernameChangePurchase($event) {
        this.selectedUsernamePurchase = $event.target.value;
    }

    showPurchaseDetails(purchase: Purchase) {
        this.selectedPurchase = purchase;
    }
}
