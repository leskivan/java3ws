import {Component, OnInit} from '@angular/core';
import {Cartitem} from "../../models/cartitem";
import {CartService} from "../../services/cart.service";
import {faTimes, faMoneyBillAlt, faCreditCard, faSpinner} from "@fortawesome/free-solid-svg-icons";
import {Product} from "../../models/product";
import {AuthService} from "../../services/auth.service";
import {PurchaseService} from "../../services/purchase.service";
import {ActivatedRoute} from "@angular/router";
import {PayPalDetail} from "../../models/paypaldetail";
import {PayPalCreate} from "../../models/paypalcreate";

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

    cartItems: Cartitem[] = [];
    icon = faTimes;
    iconCash = faMoneyBillAlt;
    iconCreditCard = faCreditCard;
    spinPayPal = false;
    disableButtons = false;
    message: string;
    payPalText = "PayPal";

    constructor(
        private cartService: CartService,
        private authService: AuthService,
        private purchaseService: PurchaseService,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.cartItems = this.cartService.getCart();
        this.activatedRoute.queryParams.subscribe(params => {
            if (params['paymentId']) {
                this.setPayPalButtonLoading();
                let ppD = new PayPalDetail(params['paymentId'], params['token'], params['PayerID'])
                this.confirmPayPal(ppD);
            }
        });
    }

    updateQuantity($event: any, item: Cartitem) {
        let quantity: number = $event.target.value;
        this.cartService.updateQuanitiy(item.product, quantity);
        this.cartItems = this.cartService.getCart();
    }

    removeFromCart(product: Product) {
        this.cartService.removeAllFromCart(product);
        this.cartItems = this.cartService.getCart();
    }

    grandTotal() {
        return this.cartService.grandTotal();
    }

    emptyCart() {
        this.cartService.emptyCart();
        this.cartItems = this.cartService.getCart();
    }

    me() {
        if (this.authService.getUserDetails()) {
            if (this.disableButtons == true) {
                return true
            }
            else {
                return false;
            }
        }
        else{
            return true;
        }
    }

    me2() {
        return this.authService.getUserDetails()
    }

    payWithCash() {
        this.purchaseService.payWithCash(this.cartItems).subscribe(
            (data) => {
                this.cartService.emptyCart();
                this.cartItems = this.cartService.getCart();
                this.message = 'Purchase successfully finished!';
                setTimeout(function () {
                    this.message = '';
                }.bind(this), 5000);
            },
            (data) => {
                console.log(data);
            }
        );
    }

    payWithPayPal() {
        this.setPayPalButtonLoading()
        this.purchaseService.payWithPayPal(this.cartItems).subscribe(
            (data: PayPalCreate) => {
                this.setPayPalButtonLoading();
                window.open(data.redirectUrl, "_self");
            },
            (data) => {
                console.log(data);
            }
        );
    }

    confirmPayPal(ppD: PayPalDetail) {
        this.purchaseService.confirmPayPal(ppD).subscribe(
            (data) => {
                this.cartService.emptyCart();
                this.cartItems = this.cartService.getCart();
                this.message = 'Purchase successfully finished!';
                setTimeout(function () {
                    this.message = '';
                }.bind(this), 5000);
                this.setPayPalButtonNormal();

            },
            (error) => {
                console.log(error)
                this.setPayPalButtonNormal();
            }
        );
    }

    setPayPalButtonLoading() {
        this.disableButtons = true;
        this.payPalText = "";
        this.spinPayPal = true;
        this.iconCreditCard = faSpinner;
    }

    setPayPalButtonNormal() {
        this.disableButtons = false;
        this.payPalText = "PayPal";
        this.spinPayPal = false;
        this.iconCreditCard = faCreditCard;
    }
}