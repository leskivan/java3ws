import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthService} from "../services/auth.service";
import * as jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class RoleGuard implements CanActivate {

    constructor(public authService: AuthService, public router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        const expectedRoles = route.data.expectedRoles;

        if (this.authService.isAuthenticated()) {
            const tokenPayload = jwt_decode(this.authService.getToken());
            for( let role of tokenPayload.scopes) {
                if (expectedRoles.includes(role)) {
                    return true;
                }
            }
        }
        this.router.navigate(['login']);
        return false;
    }
}
