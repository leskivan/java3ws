export class PayPalDetail {
    paymentId: string;
    token: string;
    payerID: string;

    constructor(paymentId: string, token: string, payerID: string) {
        this.paymentId = paymentId;
        this.token = token;
        this.payerID = payerID;
    }
}