export class LoginHistoryItem {
    createdAt: string;
    ip: string;
    username: string;

    constructor(createdAt: string, ip: string, username: string) {
        this.createdAt = createdAt;
        this.ip = ip;
        this.username = username;
    }
}