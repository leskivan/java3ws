export class Category {
    id: number;
    name: string;

    constructor(id_: number, name_: string) {
        this.name = name_;
        this.id = id_;
    }
}