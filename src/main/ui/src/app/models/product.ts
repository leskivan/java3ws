import {Category} from "./category";

export class Product {
    id: number;
    name: string;
    descripion: string;
    price: number;
    onStock: number;
    imageUrl: string;
    updatedAt: string;
    status: string;
    categories: Category[]

    constructor(id_: number, name_: string, description_: string, price_: number, onStock_: number, imageUrl_: string, updatedAt_: string, status: string, categories: Category[]) {
        this.name = name_;
        this.id = id_;
        this.descripion = description_;
        this.price = price_;
        this.onStock = onStock_;
        this.imageUrl = imageUrl_;
        this.updatedAt = updatedAt_;
        this.status = status;
        this.categories = categories;
    }


}