export class User {
    id: number;
    username: string;
    password: string;
    email: string;
    name: string;
    surname: number;
    address: number;
    city: string;
    country: string;
    zip: number;
    joinedAt: string;


    constructor(
        id: number,
        username: string,
        password: string,
        email: string,
        name: string,
        surname: number,
        address: number,
        city: string,
        country: string,
        zip: number,
        joinedAt:string
    ) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.city = city;
        this.country = country;
        this.zip = zip;
        this.joinedAt = joinedAt;
    }
}
