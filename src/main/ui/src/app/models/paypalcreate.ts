export class PayPalCreate {
    status: string;
    redirectUrl: string;
    paymentID: string;

    constructor(status: string, redirectUrl: string, paymentID: string) {
        this.status = status;
        this.redirectUrl = redirectUrl;
        this.paymentID = paymentID;
    }
}