import {User} from "./user";
import {Cartitem} from "./cartitem";

export class Purchase {
    id: number;
    user: User;
    payWay: string;
    purchaseItems: Cartitem[];
    createdAt: string;

    constructor(id: number, user: User, payWay: string, purchaseItems: Cartitem[], createdAt: string) {
        this.id = id;
        this.user = user;
        this.payWay = payWay;
        this.purchaseItems = purchaseItems;
        this.createdAt = createdAt;
    }
}