import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {User} from "../models/user";
import {AuthService} from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient, private authService: AuthService) {
    }

    baseUrl: string = this.getFullUrl(environment.USER_URL);
    baseUrlAuth: string = this.getFullUrl(environment.SIGNUP_URL);

    getUsers() {
        return this.http.get<User[]>(this.baseUrl, this.getHttpOptions());
    }

    getUserById(id: number) {
        return this.http.get<User>(this.baseUrl + '/' + id);
    }

    saveUser(newUser: User) {
        if (newUser.id == null) {
            return this.http.post(this.baseUrlAuth, newUser);
        }
        else {
            return this.http.post(this.baseUrl, newUser, this.getHttpOptions());
        }
    }
    private getFullUrl(url: string): string {
        return environment.API_ENDPOINT + url;
    }

    private getHttpOptions() {
        let httpOptions = {};
        return httpOptions = {
            headers: new HttpHeaders({
                'Authorization': this.authService.getBearer()
            })
        };
    }
}