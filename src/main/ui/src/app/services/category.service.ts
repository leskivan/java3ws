import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Category} from "../models/category";
import {environment} from '../../environments/environment';
import {AuthService} from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    constructor(private http: HttpClient, private authService: AuthService) {
    }

    baseUrl: string = this.getFullUrl(environment.CATEGORY_URL);

    getCategories() {
        return this.http.get<Category[]>(this.baseUrl);
    }

    saveCategory(category: Category) {
        return this.http.post(this.baseUrl, category, this.getHttpOptions());
    }

    deleteCategory(id: number) {
        return this.http.delete(this.baseUrl + '/' + id, this.getHttpOptions());
    }


    private getFullUrl(url: string): string {
        return environment.API_ENDPOINT + url;
    }

    private getHttpOptions() {
        let httpOptions = {};
        return httpOptions = {
            headers: new HttpHeaders({
                'Authorization': this.authService.getBearer()
            })
        };
    }

}
