import {Injectable} from '@angular/core';
import {Cartitem} from "../models/cartitem";
import {HttpClient, HttpHeaders,} from "@angular/common/http";

import {environment} from "../../environments/environment";
import {UserCredentials} from "../models/userCredentials";
import * as jwt_decode from 'jwt-decode';
import {Category} from "../models/category";
import {User} from "../models/user";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) {
    }

    baseUrl: string = this.getFullUrl("/auth/signin");
    userDetails: string = this.getFullUrl("/user/me");

    private getFullUrl(url: string): string {
        return environment.API_ENDPOINT + url;
    }

    attemptAuth(userCred: UserCredentials) {
        return this.http.post(this.baseUrl, userCred);
    }

    setToken(token: string): void {
        localStorage.setItem("token", JSON.stringify(token));
    }

    getToken(): string {
        return JSON.parse(localStorage.getItem("token"));
    }

    getBearer(): string {
        return "Bearer " + JSON.parse(localStorage.getItem("token"));
    }

    loadUserDetails() {
        const token = this.getToken();
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': this.getBearer()
            })
        };
        this.http.get<User>(this.userDetails, httpOptions).subscribe(
            (data) => {
                let user: User = new User(data.id, data.username, null, null, data.name, data.surname, data.address, data.city, data.country, data.zip, data.joinedAt)
                localStorage.setItem("user", JSON.stringify(user));
            },
            (error) => {
                console.log(error);
            }
        );
    }

    getUserDetails() {
        return JSON.parse(localStorage.getItem("user"));
    }

    logout(): void {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
    }

    getTokenExpirationDate(token: string): Date {
        const decoded = jwt_decode(token);

        if (decoded.exp === undefined) return null;

        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date;
    }

    getTokenRoles(token: string): Array<string> {
        const decoded = jwt_decode(token);
        if (decoded.scopes === undefined) return null;
        return decoded.scopes;
    }

    isAuthenticated(token?: string): boolean {
        if (!token) token = this.getToken();
        if (!token) return false;

        const date = this.getTokenExpirationDate(token);
        if (date === undefined) return true;
        return (date.valueOf() > new Date().valueOf());
    }

    isAdmin(token?: string): boolean {
        if (!token) token = this.getToken();
        if (!token) return false;
        if (this.getTokenRoles(token).includes("ROLE_ADMIN")) {
            return true;
        }
        else {
            return false;
        }
    }
}
