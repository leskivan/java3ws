import {Injectable} from '@angular/core';
import {Product} from "../models/product";
import {Cartitem} from "../models/cartitem";
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CartService {

    cartContents: Cartitem[] = [];
    baseUrl: string = this.getFullUrl(environment.CART_URL);

    constructor(private http: HttpClient) {
        this.fromStorage();
        //this.runFromServerStorage();
    }


    getCart() {
        this.fromStorage();
        return this.cartContents;
    }

    addToCart(product: Product) {
        let found = 0;
        for (let p of this.cartContents) {
            if (p.product.id == product.id) {
                p.quantity = p.quantity + 1;
                found = 1;
            }
        }
        if (!found) {
            let cartItem = new Cartitem(product, 1);
            this.cartContents.push(cartItem)
        }
        this.toStorage();
    }

    updateQuanitiy(product: Product, quantity: number) {
        for (let p of this.cartContents) {
            if (p.product.id == product.id) {
                p.quantity = quantity;
            }
            if (p.quantity < 1) {
                this.cartContents = this.cartContents.filter(obj => obj !== p);
            }
        }
        this.toStorage();
    }

    grandTotal(): number {
        let grandTotal: number = 0;
        for (let item of this.cartContents) {
            grandTotal = grandTotal + (item.product.price * item.quantity)
        }
        return grandTotal;
    }

    removeFromCart(product: Product) {
        for (let p of this.cartContents) {
            if (p.product.id == product.id) {
                p.quantity = p.quantity - 1;
            }
            if (p.quantity < 1) {
                this.cartContents = this.cartContents.filter(obj => obj !== p);
            }
        }
        this.toStorage();
    }

    removeAllFromCart(product: Product) {
        for (let p of this.cartContents) {
            if (p.product.id == product.id) {
                this.cartContents = this.cartContents.filter(obj => obj !== p);
            }
        }
        this.toStorage();
    }

    emptyCart() {
        this.cartContents = [];
        this.toStorage();
    }

    private toStorage() {
        this.toLocalStorage();
        this.toServerStorage();
    }

    private fromStorage() {
        this.fromLocalStorage();
    }

    private toServerStorage() {
        this.http.post(this.baseUrl, this.cartContents).subscribe();
    }

    private fromServerStorage() {
        return this.http.get<Cartitem[]>(this.baseUrl);
    }

    private runFromServerStorage() {
        this.fromServerStorage().subscribe(
            (data) => {
                if (data) {
                    this.cartContents = data;
                }
                else {
                    this.cartContents = [];
                }
                this.toLocalStorage();
            },
            (error) => {
                console.log(error)
            }
        );
    }

    private toLocalStorage() {
        localStorage.setItem("cart", JSON.stringify(this.cartContents));
    }

    private fromLocalStorage() {
        let localRaw = localStorage.getItem("cart");
        if (localRaw === null) {
            this.cartContents = [];
        }
        else {
            this.cartContents = JSON.parse(localRaw);
        }
    }

    private getFullUrl(url: string): string {
        return environment.API_ENDPOINT + url;
    }
}
