import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {LoginHistoryItem} from "../models/loginhistoryitem";
import {AuthService} from "./auth.service";


@Injectable({
    providedIn: 'root'
})
export class AuditService {

    constructor(private http: HttpClient, private authService: AuthService) {
    }

    baseUrl: string = this.getFullUrl(environment.AUDIT_URL);

    getAudit(dateStart: Date, dateEnd: Date) {

        const params = new HttpParams()
            .set('dtStart', dateStart.toISOString())
            .set('dtEnd', dateEnd.toISOString());

        let httpOptions = this.getHttpOptions();
        httpOptions["params"] = params;
        return this.http.get<LoginHistoryItem[]>(this.baseUrl, httpOptions);
    }

    getAuditByUsername(username: string, dateStart: Date, dateEnd: Date) {
        const params = new HttpParams()
            .set('dtStart', dateStart.toISOString())
            .set('dtEnd', dateEnd.toISOString());

        let httpOptions = this.getHttpOptions();
        httpOptions["params"] = params;
        return this.http.get<LoginHistoryItem[]>(this.baseUrl + "/" + username, httpOptions);
    }

    getMyAudit(dateStart: Date, dateEnd: Date) {
        const params = new HttpParams()
            .set('dtStart', dateStart.toISOString())
            .set('dtEnd', dateEnd.toISOString());

        let httpOptions = this.getHttpOptions();
        httpOptions["params"] = params;
        return this.http.get<LoginHistoryItem[]>(this.baseUrl + "/me", httpOptions);
    }

    private getFullUrl(url: string): string {
        return environment.API_ENDPOINT + url;
    }

    private getHttpOptions() {
        let httpOptions = {};
        return httpOptions = {
            headers: new HttpHeaders({
                'Authorization': this.authService.getBearer()
            })
        };
    }

}
