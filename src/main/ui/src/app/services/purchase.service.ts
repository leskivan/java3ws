import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Purchase} from "../models/purchase";
import {AuthService} from "./auth.service";
import {Cartitem} from "../models/cartitem";
import {User} from "../models/user";
import {PayPalDetail} from "../models/paypaldetail";

@Injectable({
    providedIn: 'root'
})
export class PurchaseService {

    constructor(private http: HttpClient, private authService: AuthService) {
    }

    baseUrl: string = this.getFullUrl(environment.PURCHASE_URL);
    baseUrlPayPal: string = this.getFullUrl(environment.PURCHASE_URL_PAY_PAL);
    baseUrlPayPalConf: string = this.getFullUrl(environment.PURCHASE_URL_PAY_PAL_CONFIRM);

    getPurchases(dateStart: Date, dateEnd: Date) {
        const params = new HttpParams()
            .set('dtStart', dateStart.toISOString())
            .set('dtEnd', dateEnd.toISOString());

        let httpOptions = this.getHttpOptions();
        httpOptions["params"] = params;
        return this.http.get<Purchase[]>(this.baseUrl, httpOptions);
    }

    getPurchasesByUsername(username: string, dateStart: Date, dateEnd: Date) {
        const params = new HttpParams()
            .set('dtStart', dateStart.toISOString())
            .set('dtEnd', dateEnd.toISOString());

        let httpOptions = this.getHttpOptions();
        httpOptions["params"] = params;
        return this.http.get<Purchase[]>(this.baseUrl + "/" + username, httpOptions);
    }

    payWithCash(cartItems: Cartitem[]) {
        let user: User = this.authService.getUserDetails();
        let purchase: Purchase = new Purchase(null, user, "Cash", cartItems, null);

        return this.http.post(this.baseUrl, purchase, this.getHttpOptions());
    }

    payWithPayPal(cartItems: Cartitem[]) {
        let user: User = this.authService.getUserDetails();
        let purchase: Purchase = new Purchase(null, user, "PayPal", cartItems, null);

        return this.http.post(this.baseUrlPayPal, purchase, this.getHttpOptions());
    }

    confirmPayPal(ppD: PayPalDetail) {
        return this.http.post(this.baseUrlPayPalConf, ppD, this.getHttpOptions());
    }

    private getFullUrl(url: string): string {
        return environment.API_ENDPOINT + url;
    }

    private getHttpOptions() {
        let httpOptions = {};
        return httpOptions = {
            headers: new HttpHeaders({
                'Authorization': this.authService.getBearer()
            })
        };
    }
}
