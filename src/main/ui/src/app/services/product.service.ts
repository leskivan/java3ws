import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Product} from "../models/product";
import {AuthService} from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class ProductService {

    constructor(private http: HttpClient, private authService: AuthService) {
    }

    baseUrl: string = this.getFullUrl(environment.PRODUCT_URL);

    getProducts() {
        return this.http.get<Product[]>(this.baseUrl);
    }

    getAllProducts() {
        return this.http.get<Product[]>(this.baseUrl + "/all", this.getHttpOptions() );
    }

    getProductById(id: number) {
        return this.http.get<Product>(this.baseUrl + '/' + id);
    }

    saveProduct(product: Product) {
        return this.http.post(this.baseUrl, product, this.getHttpOptions());
    }

    deleteProduct(id: number) {
        return this.http.delete(this.baseUrl + '/' + id, this.getHttpOptions());
    }

    private getFullUrl(url: string): string {
        return environment.API_ENDPOINT + url;
    }

    private getHttpOptions() {
        let httpOptions = {};
        return httpOptions = {
            headers: new HttpHeaders({
                'Authorization': this.authService.getBearer()
            })
        };
    }
}
