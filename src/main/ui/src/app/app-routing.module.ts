import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TestComponent} from './view/test/test.component';
import {CategoryComponent} from './view/category/category.component';
import {ProductComponent} from "./view/product/product.component";
import {CartComponent} from "./view/cart/cart.component";
import {LoginFormComponent} from "./view/user/login-form/login-form.component";
import {AuthGuard} from "./_guards/auth.guard";
import {RoleGuard} from "./_guards/role.guard";
import {ProfileComponent} from "./view/user/profile/profile.component";
import {AdminComponent} from "./view/admin/admin.component";
import {ShopComponent} from "./view/product/shop/shop.component";
import {ProductDetailsComponent} from "./view/product/product-details/product-details.component";
import {UserRegisterComponent} from "./view/user/user-register/user-register.component";

const routes: Routes = [
    {
        path: 'test',
        component: TestComponent
    },
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [RoleGuard],
        data: {
            expectedRoles: [
                'ROLE_ADMIN'
            ]
        }
    },
    {
        path: '',
        component: ShopComponent
    },
    {
        path: 'product/:id',
        component: ProductDetailsComponent
    },
    {
        path: 'cart',
        component: CartComponent
    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginFormComponent
    },
    {
        path: 'register',
        component: UserRegisterComponent
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule {
}