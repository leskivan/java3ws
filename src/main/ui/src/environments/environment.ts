export const environment = {
    production: false,
    API_ENDPOINT: 'http://localhost:8080/api',
    AppName: 'Java 3 WebShop',
    CATEGORY_URL: '/category',
    PRODUCT_URL: '/product',
    PURCHASE_URL: '/purchase',
    USER_URL: '/user',
    AUDIT_URL: '/audit',
    CART_URL: '/cart',
    SIGNUP_URL: '/auth/signup',
    PURCHASE_URL_PAY_PAL: '/paypal/make/payment',
    PURCHASE_URL_PAY_PAL_CONFIRM: '/paypal/complete/payment'
};
