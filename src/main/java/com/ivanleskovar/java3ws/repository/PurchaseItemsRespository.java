package com.ivanleskovar.java3ws.repository;

import com.ivanleskovar.java3ws.model.PurchaseItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseItemsRespository  extends JpaRepository<PurchaseItems, Long> {
}