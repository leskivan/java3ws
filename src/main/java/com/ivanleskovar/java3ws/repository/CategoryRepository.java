package com.ivanleskovar.java3ws.repository;

import com.ivanleskovar.java3ws.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
