package com.ivanleskovar.java3ws.repository;

import com.ivanleskovar.java3ws.model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {
    Optional<List<Purchase>> findByUserId(Long id);
    Optional<Purchase> findByTransactionId(String transactionId);

/*    @Query("SELECT t FROM LoginAudit t WHERE t.createdAt >= ?1 AND t.createdAt <= ?2 AND t.username= ?3")
    List<LoginAudit> findBetweenDatesbyUser(Instant dtStart, Instant dtEnd, String username);*/
}