package com.ivanleskovar.java3ws.repository;

import com.ivanleskovar.java3ws.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}