package com.ivanleskovar.java3ws.repository;

import com.ivanleskovar.java3ws.model.Role;
import com.ivanleskovar.java3ws.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
