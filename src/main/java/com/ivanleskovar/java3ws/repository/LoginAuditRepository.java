package com.ivanleskovar.java3ws.repository;

import com.ivanleskovar.java3ws.model.audit.LoginAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;

public interface LoginAuditRepository  extends JpaRepository<LoginAudit, Long>{
    @Query("SELECT t FROM LoginAudit t WHERE t.createdAt >= ?1 AND t.createdAt <= ?2")
    List<LoginAudit> findBetweenDates(Instant dtStart, Instant dtEnd);

    @Query("SELECT t FROM LoginAudit t WHERE t.createdAt >= ?1 AND t.createdAt <= ?2 AND t.username= ?3")
    List<LoginAudit> findBetweenDatesbyUser(Instant dtStart, Instant dtEnd, String username);
}
