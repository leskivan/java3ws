package com.ivanleskovar.java3ws.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PayPayException extends RuntimeException {
    private String resourceName;

    public PayPayException(String resourceName) {
        super(String.format("%s", resourceName));
        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }
}
