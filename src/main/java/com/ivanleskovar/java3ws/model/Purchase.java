package com.ivanleskovar.java3ws.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ivanleskovar.java3ws.model.audit.DateAudit;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "purchases")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({"updatedAt", "hibernateLazyInitializer", "handler"})
public class Purchase extends DateAudit {

    public Purchase() {
    }

    public Purchase(User user, PayWay payWay) {
        this.user = user;
        this.payWay = payWay;
    }


    public Purchase(User user, PayWay payWay, PurchaseStatus status) {
        this.user = user;
        this.payWay = payWay;
        this.status = status;
    }

    public Purchase(User user, PayWay payWay, PurchaseStatus status, String transactionId) {
        this.user = user;
        this.payWay = payWay;
        this.status = status;
        this.transactionId = transactionId;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    private PayWay payWay;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    private PurchaseStatus status;

    @Size(max = 100)
    private String transactionId;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<PurchaseItems> purchaseItems = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PayWay getPayWay() {
        return payWay;
    }

    public void setPayWay(PayWay payWay) {
        this.payWay = payWay;
    }

    public List<PurchaseItems> getPurchaseItems() {
        return purchaseItems;
    }

    public void setgetPurchaseItems(List<PurchaseItems> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }

    public PurchaseStatus getStatus() {
        return status;
    }

    public void setStatus(PurchaseStatus status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
