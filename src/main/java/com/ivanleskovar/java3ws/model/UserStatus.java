package com.ivanleskovar.java3ws.model;

public enum UserStatus {
    Active,
    Disabled
}
