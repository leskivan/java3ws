package com.ivanleskovar.java3ws.model;

public enum PurchaseStatus {
    New,
    Created,
    Confirmed,
    PayPalCreated,
    PayPalConfirmed
}
