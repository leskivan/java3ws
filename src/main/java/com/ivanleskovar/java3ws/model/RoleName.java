package com.ivanleskovar.java3ws.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
