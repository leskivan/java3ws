package com.ivanleskovar.java3ws.model;

import com.ivanleskovar.java3ws.exception.PayPayException;
import com.ivanleskovar.java3ws.payload.PayPalConfirmPayload;
import com.ivanleskovar.java3ws.payload.PayPalDonePayload;
import com.ivanleskovar.java3ws.payload.PayPalPaymentConfirm;
import com.ivanleskovar.java3ws.payload.PayPalPaymentCreate;
import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class PayPalClient {

    @Value("${app.payPalCallback}")
    private String callbackUrl;

    @Value("${app.payPalCallbackCancel}")
    private String callbackUrlCancel;

    @Value("${app.payPalClientId}")
    private String clientId;
    @Value("${app.payPalSecret}")
    private String clientSecret;

    public PayPalConfirmPayload createPayment(PayPalPaymentCreate pppC, HttpServletRequest request) {
        PayPalConfirmPayload response = new PayPalConfirmPayload();

        Amount amount = new Amount();
        amount.setCurrency(pppC.getCurrency());
        amount.setTotal(pppC.getSum());
        System.out.println(pppC.getSum());

        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl(callbackUrl + "/cart");
        redirectUrls.setReturnUrl(callbackUrlCancel + "/cart");

        payment.setRedirectUrls(redirectUrls);
        Payment createdPayment;
        try {
            String redirectUrl = "";
            APIContext context = new APIContext(clientId, clientSecret, "sandbox");
            createdPayment = payment.create(context);
            if (createdPayment != null) {
                List<Links> links = createdPayment.getLinks();
                for (Links link : links) {
                    if (link.getRel().equals("approval_url")) {
                        redirectUrl = link.getHref();
                        break;
                    }
                }
                response.setPaymentID(createdPayment.getId());
                response.setStatus("success");
                response.setRedirectUrl(redirectUrl);
            }
        } catch (PayPalRESTException e) {
            throw new PayPayException(e.getMessage());
        }
        return response;
    }

    public PayPalDonePayload completePayment(PayPalPaymentConfirm req){
        PayPalDonePayload response = new PayPalDonePayload();

        Payment payment = new Payment();
        payment.setId(req.getPaymentId());

        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(req.getPayerID());
        try {
            APIContext context = new APIContext(clientId, clientSecret, "sandbox");
            Payment createdPayment = payment.execute(context, paymentExecution);
            if(createdPayment!=null){
                System.out.println("Uspjeh");
                response.setPayment(createdPayment);
                response.setStatus("success");
            }
        } catch (PayPalRESTException e) {
            throw new PayPayException(e.getMessage());
        }
        return response;
    }
}
