package com.ivanleskovar.java3ws.model;

public enum PayWay {
    Cash,
    PayPal
}
