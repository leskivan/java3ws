package com.ivanleskovar.java3ws.model;

public enum ProductStatus {
    Active,
    Disabled
}
