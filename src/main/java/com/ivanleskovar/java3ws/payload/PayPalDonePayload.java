package com.ivanleskovar.java3ws.payload;

import com.paypal.api.payments.Payment;

public class PayPalDonePayload {
    private String status;
    private Payment payment;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
