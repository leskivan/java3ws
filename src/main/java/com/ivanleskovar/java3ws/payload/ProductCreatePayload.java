package com.ivanleskovar.java3ws.payload;

import com.ivanleskovar.java3ws.model.Category;
import com.ivanleskovar.java3ws.model.Product;

import java.util.Set;

public class ProductCreatePayload {
    private Product product;
    private Set<Category> categories;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }
}
