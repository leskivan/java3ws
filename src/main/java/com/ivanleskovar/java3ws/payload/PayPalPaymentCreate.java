package com.ivanleskovar.java3ws.payload;

public class PayPalPaymentCreate {
    private String sum;
    private String currency = "USD";

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getCurrency() {
        return currency;
    }
}
