package com.ivanleskovar.java3ws.controller;

import com.ivanleskovar.java3ws.exception.ResourceNotFoundException;
import com.ivanleskovar.java3ws.model.Category;
import com.ivanleskovar.java3ws.model.Product;
import com.ivanleskovar.java3ws.model.ProductStatus;
import com.ivanleskovar.java3ws.model.RoleName;
import com.ivanleskovar.java3ws.payload.ProductCreatePayload;
import com.ivanleskovar.java3ws.repository.CategoryRepository;
import com.ivanleskovar.java3ws.repository.ProductRepository;
import com.ivanleskovar.java3ws.security.CurrentUser;
import com.ivanleskovar.java3ws.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping
    public List<Product> getProducts() {
        List<Product> products = productRepository.findAll();

        products = products.stream()
                .filter(product -> !ProductStatus.Disabled.equals(product.getStatus()))
                .collect(Collectors.toList());
        return products;
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }


    @GetMapping("/{productId}")
    public Product getProductById(@PathVariable Long productId) {
        return productRepository.getOne(productId);
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Product createProduct(@RequestBody Product product) {
        return productRepository.save(product);
    }

    @DeleteMapping("/{productId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> deleteProduct(@PathVariable Long productId) {
        return productRepository.findById(productId)
                .map(product -> {
                    product.setStatus(product.getStatus() == ProductStatus.Active ?  ProductStatus.Disabled : ProductStatus.Active);
                    productRepository.save(product);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Product", "productId", productId));
    }
}
