package com.ivanleskovar.java3ws.controller;

import com.ivanleskovar.java3ws.exception.ResourceNotFoundException;
import com.ivanleskovar.java3ws.model.*;
import com.ivanleskovar.java3ws.payload.PayPalConfirmPayload;
import com.ivanleskovar.java3ws.payload.PayPalDonePayload;
import com.ivanleskovar.java3ws.payload.PayPalPaymentConfirm;
import com.ivanleskovar.java3ws.payload.PayPalPaymentCreate;
import com.ivanleskovar.java3ws.repository.ProductRepository;
import com.ivanleskovar.java3ws.repository.PurchaseRepository;
import com.ivanleskovar.java3ws.repository.UserRepository;
import com.ivanleskovar.java3ws.security.CurrentUser;
import com.ivanleskovar.java3ws.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

@RestController
@RequestMapping("/api/paypal")
public class PayPalController {

    private final PayPalClient payPalClient;

    @Autowired
    PayPalController(PayPalClient payPalClient) {
        this.payPalClient = payPalClient;
    }

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    PurchaseRepository purchaseRepository;

    @PostMapping("/make/payment")
    public PayPalConfirmPayload makePayment(@RequestBody Purchase purchase, @CurrentUser UserPrincipal currentUser, HttpServletRequest request) {
        User user = userRepository.findByUsername(currentUser.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

        Purchase newPurchase = new Purchase(user, purchase.getPayWay());

        Double totalAmount = new Double(0);

        for (PurchaseItems purchaseItems : purchase.getPurchaseItems()) {
            Product product = productRepository.getOne((long) purchaseItems.getProduct().getId());
            PurchaseItems newPurchaseItems = new PurchaseItems(product, purchaseItems.getQuantity());
            newPurchase.getPurchaseItems().add(newPurchaseItems);
            totalAmount = totalAmount + (newPurchaseItems.getProduct().getPrice() * newPurchaseItems.getQuantity());
        }
        totalAmount = round(totalAmount, 2);
        purchase.setStatus(PurchaseStatus.New);

        PayPalPaymentCreate pppC = new PayPalPaymentCreate();
        pppC.setSum(totalAmount.toString());

        PayPalConfirmPayload ppcP = payPalClient.createPayment(pppC, request);
        purchase.setStatus(PurchaseStatus.Created);
        purchase.setTransactionId(ppcP.getPaymentID());
        purchaseRepository.save(purchase);

        return ppcP;
    }

    @PostMapping("/complete/payment")
    public Purchase completePayment(@RequestBody PayPalPaymentConfirm request) {

        PayPalDonePayload ppDp = payPalClient.completePayment(request);

        Purchase purchase = purchaseRepository.findByTransactionId(ppDp.getPayment().getId())
                .orElseThrow(() -> new ResourceNotFoundException("PayPal", "transactinId", ppDp.getPayment().getId()));

        purchase.setStatus(PurchaseStatus.Confirmed);
        purchaseRepository.save(purchase);
        return purchase;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}