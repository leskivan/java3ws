package com.ivanleskovar.java3ws.controller;

import com.ivanleskovar.java3ws.model.audit.LoginAudit;
import com.ivanleskovar.java3ws.repository.LoginAuditRepository;
import com.ivanleskovar.java3ws.security.CurrentUser;
import com.ivanleskovar.java3ws.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@RestController
public class UserAuditController {
    @Autowired
    private LoginAuditRepository loginAuditRepository;

    @GetMapping("/api/audit/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<LoginAudit> getAllLoginAudit(
            @PathVariable(value = "username") String username,
            @RequestParam(value = "dtStart") Optional<Instant> dtStartParam,
            @RequestParam(value = "dtEnd") Optional<Instant> dtEndParam
    ) {
        Instant dtStart = dtStartParam.orElse(Instant.ofEpochSecond(0));
        Instant dtEnd = dtEndParam.orElse(Instant.now());
        return loginAuditRepository.findBetweenDatesbyUser(dtStart, dtEnd, username);
    }

    @GetMapping("/api/audit/me")
    public List<LoginAudit> getAllMyLoginAudit(
            @RequestParam(value = "dtStart") Optional<Instant> dtStartParam,
            @RequestParam(value = "dtEnd") Optional<Instant> dtEndParam,
            @CurrentUser UserPrincipal currentUser
    )
    {
        Instant dtStart = dtStartParam.orElse(Instant.ofEpochSecond(0));
        Instant dtEnd = dtEndParam.orElse(Instant.now());
        return loginAuditRepository.findBetweenDatesbyUser(dtStart, dtEnd, currentUser.getUsername());
    }
}
