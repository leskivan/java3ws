package com.ivanleskovar.java3ws.controller;

import com.ivanleskovar.java3ws.exception.ResourceNotFoundException;
import com.ivanleskovar.java3ws.model.RoleName;
import com.ivanleskovar.java3ws.model.User;
import com.ivanleskovar.java3ws.payload.UserProfile;
import com.ivanleskovar.java3ws.payload.UserSummary;
import com.ivanleskovar.java3ws.repository.UserRepository;
import com.ivanleskovar.java3ws.security.CurrentUser;
import com.ivanleskovar.java3ws.security.UserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/user/me")
    public UserProfile getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        User user = userRepository.findByUsername(currentUser.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

        UserProfile userProfile = new UserProfile(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getName(),
                user.getSurname(),
                user.getAddress(),
                user.getCity(),
                user.getZip(),
                user.getCountry(),
                user.getCreatedAt()
        );

        return userProfile;
    }

    @GetMapping("/user/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        UserProfile userProfile = new UserProfile(
                user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getName(),
                user.getSurname(),
                user.getAddress(),
                user.getCity(),
                user.getZip(),
                user.getCountry(),
                user.getCreatedAt()
        );

        return userProfile;
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserProfile> getUsers() {
        List<UserProfile> userProfiles = new ArrayList<>();
        List<User> users = userRepository.findAll();
        for (User user : users) {
            UserProfile userProfile = new UserProfile(
                    user.getId(),
                    user.getUsername(),
                    user.getEmail(),
                    user.getName(),
                    user.getSurname(),
                    user.getAddress(),
                    user.getCity(),
                    user.getZip(),
                    user.getCountry(),
                    user.getCreatedAt()
            );
            userProfiles.add(userProfile);
        }

        return userProfiles;
    }

    @PostMapping("/user")
    public User updateUser(@CurrentUser UserPrincipal currentUser, HttpServletResponse httpResponse,  HttpServletRequest request, @RequestBody User user) {
        if (request.isUserInRole(RoleName.ROLE_ADMIN.toString()) || currentUser.getUsername().equals(user.getUsername())) {
            User userFromDb = userRepository.getOne(user.getId());
            user.setPassword(userFromDb.getPassword());
            user.setRoles(userFromDb.getRoles());
            user.setCreatedAt(userFromDb.getCreatedAt());
            userRepository.save(user);

        }
        else {
            try {
                httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Forbidden");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
