package com.ivanleskovar.java3ws.controller;

import com.ivanleskovar.java3ws.exception.ResourceNotFoundException;
import com.ivanleskovar.java3ws.model.*;
import com.ivanleskovar.java3ws.repository.ProductRepository;
import com.ivanleskovar.java3ws.repository.PurchaseRepository;
import com.ivanleskovar.java3ws.repository.UserRepository;
import com.ivanleskovar.java3ws.security.CurrentUser;
import com.ivanleskovar.java3ws.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/purchase")
public class PurchaseController {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    PurchaseRepository purchaseItemsRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    UserRepository userRepository;

    @PostMapping
    public Purchase postSomething(@RequestBody Purchase purchase, @CurrentUser UserPrincipal currentUser) {
        User user = userRepository.findByUsername(currentUser.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

        Purchase newPurchase = new Purchase(user, purchase.getPayWay());

        for (PurchaseItems purchaseItems : purchase.getPurchaseItems()) {
            Product product = productRepository.getOne((long) purchaseItems.getProduct().getId());
            PurchaseItems newPurchaseItems = new PurchaseItems(product, purchaseItems.getQuantity());
            newPurchase.getPurchaseItems().add(newPurchaseItems);
        }
        purchase.setStatus(PurchaseStatus.Confirmed);
        purchaseRepository.save(purchase);
        return purchase;
    }

    @GetMapping
    public List<Purchase> getPurchases(
            @CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "dtStart") Optional<Instant> dtStartParam,
            @RequestParam(value = "dtEnd") Optional<Instant> dtEndParam
    ) {
        Instant dtStart = dtStartParam.orElse(Instant.ofEpochSecond(0));
        Instant dtEnd = dtEndParam.orElse(Instant.now());

        User u1 = userRepository.findByUsername(currentUser.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

        List<Purchase> purchaseList = purchaseRepository.findByUserId(u1.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Purchase", "username", currentUser.getUsername()));


        purchaseList = purchaseList.stream()
                .filter(dates -> dates.getStatus() == PurchaseStatus.Confirmed && Date.from(dates.getCreatedAt()).after(Date.from(dtStart)) && Date.from(dates.getCreatedAt()).before(Date.from(dtEnd)))
                .collect(Collectors.toList());


        return purchaseList;
    }

    @GetMapping("/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Purchase> getPurchseByUsername(
            @PathVariable(value = "username") String username,
            @RequestParam(value = "dtStart") Optional<Instant> dtStartParam,
            @RequestParam(value = "dtEnd") Optional<Instant> dtEndParam
    ) {

        Instant dtStart = dtStartParam.orElse(Instant.ofEpochSecond(0));
        Instant dtEnd = dtEndParam.orElse(Instant.now());

        User u1 = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        List<Purchase> purchaseList = purchaseRepository.findByUserId(u1.getId()).orElse(new ArrayList<>());

        purchaseList = purchaseList.stream()
                .filter(dates -> dates.getStatus() == PurchaseStatus.Confirmed && Date.from(dates.getCreatedAt()).after(Date.from(dtStart)) && Date.from(dates.getCreatedAt()).before(Date.from(dtEnd)))
                .collect(Collectors.toList());
        return purchaseList;

    }

}
