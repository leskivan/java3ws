package com.ivanleskovar.java3ws.controller;

import com.ivanleskovar.java3ws.model.Cartitem;
import com.ivanleskovar.java3ws.payload.ProductCreatePayload;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartController {

    @GetMapping
    public List<Cartitem> getCart(HttpSession httpSession) {
        return (List<Cartitem>) httpSession.getAttribute("cart");
    }

    @PostMapping
    public void updateCart(HttpSession httpSession, @RequestBody List<Cartitem> cartitems) {
        httpSession.setAttribute("cart", cartitems);
    }
}
