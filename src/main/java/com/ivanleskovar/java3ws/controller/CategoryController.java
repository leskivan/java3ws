package com.ivanleskovar.java3ws.controller;

import com.ivanleskovar.java3ws.exception.ResourceNotFoundException;
import com.ivanleskovar.java3ws.model.Category;
import com.ivanleskovar.java3ws.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping
    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Category createCategory(@RequestBody Category cat) {
        return categoryRepository.save(cat);
    }

    @DeleteMapping("/{categoryId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> deleteCategory(@PathVariable Long categoryId) {
        return categoryRepository.findById(categoryId)
                .map(question -> {
                    categoryRepository.delete(question);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() ->  new ResourceNotFoundException("Category", "categoryId", categoryId));
    }
}
